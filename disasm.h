#ifndef __DISASM_H__
#define __DISASM_H__

#include <windows.h>
#include "nanomite.h"

#define MAXCMDSIZE 16

struct REFERENCE
{
    unsigned int addr;
    unsigned int dest;
};

unsigned int* RichDisasm(HWND richedit, unsigned char* mem, unsigned int mem_va, unsigned int codebase, unsigned int code_size, int ip);
unsigned int* SilentDisasm(unsigned char* mem, unsigned int mem_va, unsigned int codebase, unsigned int code_size, int ip);
bool BuildReferenceTable(NANO_TABLE* NanoTable, unsigned char* codepage_, unsigned int codesize, unsigned int codeva);
bool BuildFullReferenceTable(NANO_TABLE* NanoTable, unsigned char* codepage_, unsigned int codesize, unsigned int codeva);
bool FindReference(unsigned int destva);

#endif // __DISASM_H__
