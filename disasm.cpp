#define _WIN32_WINNT 0x0501
#define WINVER 0x0501
#define _WIN32_IE 0x0500
#define BEA_ENGINE_STATIC
#include <windows.h>
#include <stdio.h>
#include "disasm.h"
#include "_global.h"
#include "BeaEngine/BeaEngine.h"

#define rtf_start "{\\rtf1{\\fonttbl{\\f0\\fnil Fixedsys;}}{\\colortbl;\\red0\\green0\\blue0;\\red0\\green0\\blue128;\\red0\\green128\\blue0;\\red0\\green128\\blue128;\\red128\\green0\\blue0;\\red128\\green0\\blue128;\\red128\\green128\\blue0;\\red192\\green192\\blue192;\\red128\\green128\\blue128;\\red0\\green0\\blue255;\\red0\\green255\\blue0;\\red0\\green255\\blue255;\\red255\\green0\\blue0;\\red255\\green0\\blue255;\\red255\\green255\\blue0;\\red255\\green255\\blue255;\\red255\\green251\\blue240;}"

/*
colors:
01:\\red0\\green0\\blue0
02:\\red0\\green0\\blue128
03:\\red0\\green128\\blue0
04:\\red0\\green128\\blue128
05:\\red128\\green0\\blue0
06:\\red128\\green0\\blue128
07:\\red128\\green128\\blue0
08:\\red192\\green192\\blue192
09:\\red128\\green128\\blue128
10:\\red0\\green0\\blue255
11:\\red0\\green255\\blue0
12:\\red0\\green255\\blue255
13:\\red255\\green0\\blue0
14:\\red255\\green0\\blue255
15:\\red255\\green255\\blue0
16:\\red255\\green255\\blue255
17:\\red255\\green251\\blue240
*/

static int PrintArgument(char* instr, ARGTYPE* Argument, INSTRTYPE* Instruction, bool* had_arg)
{
    int text_size=0;
    const char* prepcomma="\0, ";
    const char* endhighlight="\0}";
    char highlight[100]="";
    bool do_highlight=false;

    int argtype=Argument->ArgType;
    int brtype=Instruction->BranchType;
    char* argmnemonic=Argument->ArgMnemonic;
    char segment[3]="";
    switch(Argument->SegmentReg)
    {
    case ESReg:
        strcpy(segment, "es");
        break;
    case DSReg:
        strcpy(segment, "ds");
        break;
    case FSReg:
        strcpy(segment, "fs");
        break;
    case GSReg:
        strcpy(segment, "gs");
        break;
    case CSReg:
        strcpy(segment, "cs");
        break;
    case SSReg:
        strcpy(segment, "ss");
        break;
    default:
        strcpy(segment, "??");
        break;
    }

    if(argtype!=NO_ARGUMENT and argmnemonic[0])
    {
        if(argtype&MEMORY_TYPE)
        {
            int basereg=Argument->Memory.BaseRegister;
            if(basereg&REG4 or basereg&REG5) //esp or ebp
            {
                strcpy(highlight+1, "{\\highlight12 ");
                do_highlight=true;
                //Highlight ESP or EBP memory move (\highlight12)
            }
            else
            {
                strcpy(highlight+1, "{\\cf2 ");
                do_highlight=true;
            }
            switch(Argument->ArgSize)
            {
            case 8:
                text_size+=sprintf(instr, "%s%sbyte ptr %s:[%s]%s", prepcomma+*had_arg, highlight+do_highlight, segment, argmnemonic, endhighlight+do_highlight);
                break;
            case 16:
                text_size+=sprintf(instr, "%s%sword ptr %s:[%s]%s", prepcomma+*had_arg, highlight+do_highlight, segment, argmnemonic, endhighlight+do_highlight);
                break;
            case 32:
                text_size+=sprintf(instr, "%s%sdword ptr %s:[%s]%s", prepcomma+*had_arg, highlight+do_highlight, segment, argmnemonic, endhighlight+do_highlight);
                break;
            case 64:
                text_size+=sprintf(instr, "%s%sqword ptr %s:[%s]%s", prepcomma+*had_arg, highlight+do_highlight, segment, argmnemonic, endhighlight+do_highlight);
                break;
            }
        }
        else
        {
            if(brtype and brtype!=RetType and !(argtype&REGISTER_TYPE))
            {
                strcpy(highlight+1, "{\\highlight15 ");
                do_highlight=true;
                //printf("%.8X\n", Instruction->Opcode);
                unsigned char* opc=(unsigned char*)&Instruction->Opcode;
                if(*opc==0xEB or Instruction->Opcode<0x80)
                {
                    strcat(highlight+1, "short ");
                    //short jumps
                }
                //Highlight (un)condentional jumps and calls (\highlight15)
            }
            text_size+=sprintf(instr, "%s%s%s%s", prepcomma+*had_arg, highlight+do_highlight, argmnemonic, endhighlight+do_highlight);
        }
        *had_arg=true;
    }
    return text_size;
}

static int PrintInstruction(char* instr, DISASM* MyDisasm)
{
    int text_size=0;
    int brtype=MyDisasm->Instruction.BranchType;
    char mnemonic[16]="";
    strcpy(mnemonic, MyDisasm->Instruction.Mnemonic);
    int len=strlen(mnemonic);
    if(mnemonic[len-1]==' ')
        mnemonic[len-1]=0;
    bool do_highlight=false;
    const char* endhighlight="\0}";
    char highlight[100]="";
    if(brtype)
    {
        if(brtype==RetType or brtype==CallType)
        {
            strcpy(highlight+1, "{\\highlight12 ");
            do_highlight=true;
            //calls and rets
        }
        else if(brtype==JmpType)
        {
            strcpy(highlight+1, "{\\highlight15 ");
            do_highlight=true;
            //uncond jumps
        }
        else
        {
            strcpy(highlight+1, "{\\highlight15\\cf13 ");
            do_highlight=true;
            //cond jumps
        }
    }
    else if(!strcasecmp(mnemonic, "push") or !strcasecmp(mnemonic, "pop"))
    {
        strcpy(highlight+1, "{\\cf10 ");
        do_highlight=true;
        //push/pop
    }
    else if(!strcasecmp(mnemonic, "nop") or !strcasecmp(mnemonic, "int3"))
    {
        strcpy(highlight+1, "{\\cf9 ");
        do_highlight=true;
        //nop
    }
    text_size+=sprintf(instr, "%s%s%s ", highlight+do_highlight, mnemonic, endhighlight+do_highlight);
    return text_size;
}

static int PrintRtfInstruction(char* instr, DISASM* MyDisasm, int instr_len, bool iseip, bool* ismiddle)
{
    int text_size=0;
    bool had_arg=false;

    char bytes[50]="";
    int len=0;
    unsigned char* opcodes=(unsigned char*)MyDisasm->EIP;
    for(int i=0; i<instr_len; i++)
        len+=sprintf(bytes+len, "%.2X", opcodes[i]);
    bytes[16]=0;
    char middle[50]="   ";
    if(*ismiddle and strstr(bytes, "CC"))
    {
        strcpy(middle, " {\\highlight13  } ");
        *ismiddle=false;
    }
    if(len<16)
        memset(bytes+len, ' ', 16-len);
    if(!iseip)
        text_size+=sprintf(instr+text_size, "%.8X|%s%s | ", (unsigned int)MyDisasm->VirtualAddr, middle, bytes);
    else
        text_size+=sprintf(instr+text_size, "{\\highlight1\\cf17 %.8X}|%s%s | ", (unsigned int)MyDisasm->VirtualAddr, middle, bytes);

    if(MyDisasm->Prefix.LockPrefix)
        text_size+=sprintf(instr+text_size, "lock ");
    if(MyDisasm->Prefix.RepPrefix)
        text_size+=sprintf(instr+text_size, "rep ");
    if(MyDisasm->Prefix.RepnePrefix)
        text_size+=sprintf(instr+text_size, "repne ");
    text_size+=PrintInstruction(instr+text_size, MyDisasm);
    text_size+=PrintArgument(instr+text_size, &MyDisasm->Argument1, &MyDisasm->Instruction, &had_arg);
    text_size+=PrintArgument(instr+text_size, &MyDisasm->Argument2, &MyDisasm->Instruction, &had_arg);
    text_size+=PrintArgument(instr+text_size, &MyDisasm->Argument3, &MyDisasm->Instruction, &had_arg);
    if(!had_arg) //Remove space after instrs without arg
        text_size--;
    //printf("%s\\line\n", instr);
    text_size+=sprintf(instr+text_size, "\\line\r\n");

    return text_size;
}

static unsigned int Disassembleback(char* memblock, unsigned int codebase, unsigned int code_size, unsigned int ip, int n)
{
    int i,cmdsize;
    unsigned int abuf[131],addr,back;

    DISASM da;
    memset(&da, 0, sizeof(DISASM));

    if(!memblock)
        return 0; //Error, no code!
    if(n<0)
        n=0;
    else if(n>127)
        n=127; //Try to correct obvious errors
    if(ip>codebase+code_size)
        ip=codebase+code_size;
    if(n==0)
        return ip; //Obvious answers
    if(ip<=codebase+n)
        return codebase;
    back=MAXCMDSIZE*(n+3); //Command length limited to MAXCMDSIZE
    if(ip<codebase+back)
        back=ip-codebase;
    addr=ip-back;
    da.EIP=(UIntPtr)memblock+(addr-codebase);
    for(i=0; addr<ip; i++)
    {
        abuf[i%128]=addr;
        cmdsize=Disasm(&da);
        if(cmdsize==UNKNOWN_OPCODE) //unknown opcodes (default=2 bytes)
            cmdsize=2;
        da.EIP+=cmdsize;
        addr+=cmdsize;
        back-=cmdsize;
    }
    if(i<n)
        return abuf[0];
    else
        return abuf[(i-n+128)%128];
}

unsigned int* SilentDisasm(unsigned char* mem, unsigned int mem_va, unsigned int codebase, unsigned int code_size, int ip)
{
    static unsigned int addrs[31];
    memset(addrs, 0, sizeof(addrs));
    unsigned int first=Disassembleback((char*)mem, codebase, code_size, ip, 16); //16 instructions back
    addrs[0]=first+mem_va+codebase;
    DISASM MyDisasm;
    int len, i=0,j=1;
    memset(&MyDisasm, 0, sizeof(DISASM));
    MyDisasm.VirtualAddr=mem_va+codebase+first;
    MyDisasm.EIP=(UIntPtr)mem+first;
    while(i<33)
    {
        len=Disasm(&MyDisasm);
        if(len==UNKNOWN_OPCODE)
            len=2;
        MyDisasm.EIP+=len;
        addrs[j]=addrs[j-1]+len;
        MyDisasm.VirtualAddr+=len;
        j++;
        i++;
    }
    return addrs;
}

unsigned int* RichDisasm(HWND richedit, unsigned char* mem, unsigned int mem_va, unsigned int codebase, unsigned int code_size, int ip)
{
    static unsigned int addrs[31];
    memset(addrs, 0, sizeof(addrs));
    unsigned int first=Disassembleback((char*)mem, codebase, code_size, ip, 16); //16 instructions back
    addrs[0]=first+mem_va+codebase;
    DISASM MyDisasm;
    int len, i=0,j=1;
    memset(&MyDisasm, 0, sizeof(DISASM));
    MyDisasm.VirtualAddr=mem_va+codebase+first;
    MyDisasm.EIP=(UIntPtr)mem+first;
    MyDisasm.Options=NoformatNumeral;
    char instr_string[256]="";
    char* massive_string=(char*)emalloc(1024*1024); //1mb
    if(!massive_string)
        return 0;
    memset(massive_string, 0, 1024*1024);
    int text_size=sprintf(massive_string, rtf_start);
    bool ismiddle=false;
    while(i<33)
    {
        bool iseip=false;
        if(MyDisasm.EIP==(UIntPtr)(mem+ip))
            iseip=true;
        len=Disasm(&MyDisasm);
        int temp_size;
        if(len==UNKNOWN_OPCODE)
            len=2;
        if(i==15)
            ismiddle=true;
        else if(i==18)
            ismiddle=false;
        temp_size=PrintRtfInstruction(instr_string, &MyDisasm, len, iseip, &ismiddle);
        strcpy(massive_string+text_size, instr_string);
        text_size+=temp_size;
        MyDisasm.EIP+=len;
        addrs[j]=addrs[j-1]+len;
        MyDisasm.VirtualAddr+=len;
        j++;
        i++;
    }
    if(massive_string[text_size-3]=='\r')
        massive_string[text_size-3]='}';
    else
        massive_string[text_size-1]='}';
    SetWindowText(richedit, massive_string);
    free(massive_string);
    return addrs;
}

static unsigned int* refs=0;
static unsigned int ref_count=0;

int FindNano(NANO_TABLE* NanoTable, unsigned int addr)
{
    for(int i=0; i<NanoTable->total; i++)
        if(NanoTable->nano[i].addr==addr)
            return i;
    return -1;
}

bool BuildReferenceTable(NANO_TABLE* NanoTable, unsigned char* codepage_, unsigned int codesize, unsigned int codeva)
{
    ref_count=0;
    unsigned char* codepage=(unsigned char*)emalloc(codesize);
    if(!codepage)
        return 0;
    memcpy(codepage, codepage_, codesize);
    DISASM da;
    memset(&da, 0, sizeof(DISASM));
    da.VirtualAddr=codeva;
    da.EIP=(UIntPtr)codepage;
    for(unsigned int i=0; i<codesize-6;)
    {
        if(codepage[i]==0xCC)
        {
            int nano=FindNano(NanoTable, da.VirtualAddr);
            if(nano!=-1)
                AssembleJump(codepage+i, &NanoTable->nano[nano], true);
        }
        int len=Disasm(&da);
        if(len==UNKNOWN_OPCODE)
            len=1;
        unsigned int addr=da.Instruction.AddrValue;
        if(addr>codeva and addr<(codeva+codesize+1))
            ref_count++;
        da.EIP+=len;
        da.VirtualAddr+=len;
        i+=len;
    }
    if(refs)
        free(refs);
    refs=(unsigned int*)emalloc(ref_count*sizeof(unsigned int));
    if(!refs)
    {
        free(codepage);
        return false;
    }

    memset(refs, 0, ref_count*sizeof(unsigned int));

    memset(&da, 0, sizeof(DISASM));
    da.VirtualAddr=codeva;
    da.EIP=(UIntPtr)codepage;
    for(unsigned int i=0,j=0; i<codesize-6;)
    {
        if(codepage[i]==0xCC)
        {
            int nano=FindNano(NanoTable, da.VirtualAddr);
            if(nano!=-1)
                AssembleJump(codepage+i, &NanoTable->nano[nano], true);
        }
        int len=Disasm(&da);
        if(len==UNKNOWN_OPCODE)
            len=1;
        unsigned int addr=da.Instruction.AddrValue;
        if(addr>codeva and addr<(codeva+codesize+1))
        {
            refs[j]=da.Instruction.AddrValue;
            j++;
        }
        da.EIP+=len;
        da.VirtualAddr+=len;
        i+=len;
    }
    free(codepage);
    return true;
}

bool BuildFullReferenceTable(NANO_TABLE* NanoTable, unsigned char* codepage_, unsigned int codesize, unsigned int codeva)
{
    ref_count=0;
    unsigned char* codepage=(unsigned char*)emalloc(codesize);
    if(!codepage)
        return 0;
    memcpy(codepage, codepage_, codesize);
    DISASM da;
    memset(&da, 0, sizeof(DISASM));
    da.VirtualAddr=codeva;
    da.EIP=(UIntPtr)codepage;
    for(unsigned int i=0; i<codesize-6; i++)
    {
        if(codepage[i]==0xCC)
        {
            int nano=FindNano(NanoTable, da.VirtualAddr);
            if(nano!=-1)
                AssembleJump(codepage+i, &NanoTable->nano[nano], true);
        }
        Disasm(&da);
        if(da.Instruction.BranchType and da.Instruction.BranchType!=RetType)
            ref_count++;
        da.EIP++;
        da.VirtualAddr++;
    }
    if(refs)
        free(refs);
    refs=(unsigned int*)emalloc(ref_count*sizeof(unsigned int));
    if(!refs)
    {
        free(codepage);
        return false;
    }

    memset(refs, 0, ref_count*sizeof(unsigned int));

    memset(&da, 0, sizeof(DISASM));
    da.VirtualAddr=codeva;
    da.EIP=(UIntPtr)codepage;
    for(unsigned int i=0,j=0; i<codesize-6; i++)
    {
        if(codepage[i]==0xCC)
        {
            int nano=FindNano(NanoTable, da.VirtualAddr);
            if(nano!=-1)
                AssembleJump(codepage+i, &NanoTable->nano[nano], true);
        }
        Disasm(&da);
        if(da.Instruction.BranchType and da.Instruction.BranchType!=RetType)
        {
            refs[j]=da.Instruction.AddrValue;
            j++;
        }
        da.EIP++;
        da.VirtualAddr++;
    }
    free(codepage);
    return true;
}

bool FindReference(unsigned int destva)
{
    if(!refs or !ref_count)
        return 0;
    for(unsigned int i=0; i<ref_count; i++)
        if(refs[i]==destva)
            return true;
    return false;
}
