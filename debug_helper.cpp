#include "debug_helper.h"
#include "TitanEngine\TitanEngine.h"

PROCESS_INFORMATION* fdProcessInfo;
HWND shared;

static char guard_text[256]="Break!";

bool FixIsDebuggerPresent(HANDLE hProcess, bool hide)
{
    if(!hProcess)
        return false;
    unsigned int peb_addr=(unsigned int)GetPEBLocation(hProcess);
    if(!peb_addr)
        return false;
    NTPEB myPeb= {0};
    if(!ReadProcessMemory(hProcess, (void*)peb_addr, &myPeb, sizeof(NTPEB), 0))
        return false;
    if(hide)
    {
        myPeb.BeingDebugged=false;
        myPeb.NtGlobalFlag=0;
    }
    else
        myPeb.BeingDebugged=true;
    if(!WriteProcessMemory(hProcess, (void*)peb_addr, &myPeb, sizeof(NTPEB), 0))
        return false;
    return true;
}

bool rpm(long addr, void* buffer, unsigned int size)
{
    return ReadProcessMemory(fdProcessInfo->hProcess, (void*)addr, buffer, size, 0);
}

bool wpm(long addr, void* buffer, unsigned int size)
{
    return WriteProcessMemory(fdProcessInfo->hProcess, (void*)addr, buffer, size, 0);
}

void FatalError(const char* msg)
{
    MessageBoxA(shared, msg, "Fatal Error!", MB_ICONERROR);
    ExitProcess(1);
}

void BreakDebugger()
{
    DWORD oldprotect;
    VirtualProtect(guard_text, 256, PAGE_READWRITE|PAGE_GUARD, &oldprotect);
    guard_text[0]=0;
}

bool dbg(const char* a)
{
    if(!strcmp(a,":"))
        return false;
    return true;
}
