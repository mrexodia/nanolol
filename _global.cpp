#include "_global.h"
#include <windows.h>

void* emalloc(size_t size)
{
    void* a=malloc(size+0x1000);
    //unsigned char* a=new unsigned char[size];
    if(!a)
    {
        MessageBoxA(0, "Could not allocate memory", "Error", MB_ICONERROR);
        ExitProcess(1);
    }
    memset(a, 0, size);
    return a;
}

void efree(void* ptr)
{
    free(ptr);
    //VirtualFree(ptr, 0, MEM_RELEASE);
    //delete[] (unsigned char*)ptr;
}
