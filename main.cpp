#define _WIN32_WINNT 0x0501
#define WINVER 0x0501
#define _WIN32_IE 0x0500
#include <windows.h>
#include <commctrl.h>
#include <stdio.h>
#include <richedit.h>
#include "resource.h"
#include "TitanEngine\TitanEngine.h"
#include "BeaEngine\BeaEngine.h"
#include "nanomite.h"
#include "disasm.h"
#include "debug_helper.h"
#include "_global.h"
#include "pefunc.h"

static HINSTANCE hInst;
static bool dropped_dump=false;
static bool dropped_packed=false;
static HANDLE br=CreateSolidBrush(GetSysColor(COLOR_3DFACE));
static NANO_TABLE* NanoTable;
static int curnano=0;
static int firstnano=0;
static unsigned char* codepage=0;
static unsigned int codestart=0;
static unsigned int codesize=0;
static unsigned int lastpress[6]= {0,0,0,0,0,0};
static bool markersonly=false;
static bool notdisassembled=false;

BOOL CALLBACK DropSubclass(HWND hwndDlg, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
    switch(uMsg)
    {
    case WM_KEYDOWN:
    {
        int key=wParam;
        if((key>=0x41 and key<=0x5A) or (key>=0x60 and key<=0x6F) or (key>=0xBA and key<=0xDF) or key==8 or key==0xD)
            MessageBoxA(hwndDlg, "Pressed key!", "lol", MB_ICONERROR);
    }
    return TRUE;

    case WM_DROPFILES:
    {
        switch(GetDlgCtrlID(hwndDlg))
        {
        case IDC_EDT_DUMPED:
        {
            DragQueryFileA((HDROP)wParam, NULL, szDumpedName, 256);
            SetWindowTextA(hwndDlg, szDumpedName);
            dropped_dump=true;
        }
        return TRUE;

        case IDC_EDT_PACKED:
        {
            int len=DragQueryFileA((HDROP)wParam, NULL, szFileName, 256);
            if(!len)
                return TRUE;
            len--;
            strcpy(szCurrentDir, szFileName);
            while(szCurrentDir[len]!='\\')
                len--;
            szCurrentDir[len]=0;
            SetCurrentDirectoryA(szCurrentDir); //set working directory to dir of packed file
            SetWindowTextA(hwndDlg, szFileName);
            dropped_packed=true;
        }
        return TRUE;
        }
    }
    break;
    }
    return CallWindowProc((WNDPROC)GetWindowLong(hwndDlg, GWL_USERDATA), hwndDlg, uMsg, wParam, lParam);
}

void SetLowerRightCorner(HWND hwndDlg)
{
    typedef struct tagNONCLIENTMETRICS2
    {
        UINT    cbSize;
        int     iBorderWidth;
        int     iScrollWidth;
        int     iScrollHeight;
        int     iCaptionWidth;
        int     iCaptionHeight;
        LOGFONT lfCaptionFont;
        int     iSmCaptionWidth;
        int     iSmCaptionHeight;
        LOGFONT lfSmCaptionFont;
        int     iMenuWidth;
        int     iMenuHeight;
        LOGFONT lfMenuFont;
        LOGFONT lfStatusFont;
        LOGFONT lfMessageFont;
        int     iPaddedBorderWidth;
    } NONCLIENTMETRICS2, *PNONCLIENTMETRICS2, *LPNONCLIENTMETRICS2;
    NONCLIENTMETRICS2 ncm;
    OSVERSIONINFO OS;
    OS.dwOSVersionInfoSize=sizeof(OSVERSIONINFO);
    GetVersionEx(&OS);
    int BorderWidth;
    if (OS.dwMajorVersion < 6)
    {
        ncm.cbSize=sizeof(ncm)-sizeof(ncm.iPaddedBorderWidth);
        SystemParametersInfo(SPI_GETNONCLIENTMETRICS, sizeof(NONCLIENTMETRICS), &ncm, 0);
        BorderWidth=ncm.iBorderWidth;
    }
    else
    {
        ncm.cbSize=sizeof(ncm);
        SystemParametersInfo(SPI_GETNONCLIENTMETRICS, sizeof(NONCLIENTMETRICS), &ncm, 0);
        BorderWidth=ncm.iBorderWidth+ncm.iPaddedBorderWidth;
    }
    RECT rc;
    SystemParametersInfo(SPI_GETWORKAREA, 0, &rc, 0);
    int xavailable=rc.right;
    int yavailable=rc.bottom;
    GetWindowRect(hwndDlg, &rc);
    int width=rc.right-rc.left;
    int height=rc.bottom-rc.top;
    SetWindowPos(hwndDlg, 0, xavailable-width-BorderWidth, yavailable-height-BorderWidth, 0, 0, SWP_NOZORDER|SWP_NOSIZE|SWP_NOACTIVATE);
}

bool BrowseFileOpen(HWND owner, const char* filter, const char* defext, char* filename, int filename_size, const char* init_dir)
{
    OPENFILENAME ofstruct;
    memset(&ofstruct, 0, sizeof(ofstruct));
    ofstruct.lStructSize=sizeof(ofstruct);
    ofstruct.hwndOwner=owner;
    ofstruct.hInstance=hInst;
    ofstruct.lpstrFilter=filter;
    ofstruct.lpstrFile=filename;
    ofstruct.nMaxFile=filename_size;
    ofstruct.lpstrInitialDir=init_dir;
    ofstruct.lpstrDefExt=defext;
    ofstruct.Flags=OFN_EXTENSIONDIFFERENT|OFN_HIDEREADONLY|OFN_NONETWORKBUTTON;
    return GetOpenFileNameA(&ofstruct);
}

bool BrowseFileSave(HWND owner, const char* filter, const char* defext, char* filename, int filename_size, const char* init_dir)
{
    OPENFILENAME ofstruct;
    memset(&ofstruct, 0, sizeof(ofstruct));
    ofstruct.lStructSize=sizeof(ofstruct);
    ofstruct.hwndOwner=owner;
    ofstruct.hInstance=hInst;
    ofstruct.lpstrFilter=filter;
    ofstruct.lpstrFile=filename;
    ofstruct.nMaxFile=filename_size;
    ofstruct.lpstrInitialDir=init_dir;
    ofstruct.lpstrDefExt=defext;
    ofstruct.Flags=OFN_EXTENSIONDIFFERENT|OFN_HIDEREADONLY|OFN_NONETWORKBUTTON|OFN_OVERWRITEPROMPT;
    return GetSaveFileNameA(&ofstruct);
}

static void TrackProgress(HWND hwndDlg)
{
    int pdone_int=((curnano+1)*100)/NanoTable->total;
    SendDlgItemMessageA(hwndDlg, IDC_PROGRESS1, PBM_SETPOS, pdone_int, 0);
}

static bool ContainsAddr(unsigned int* addrs, unsigned int addr, int size)
{
    for(int i=0; i<size; i++)
        if(addrs[i]==addr)
            return true;
    return false;
}

void EditSelChange(HWND hwndDlg)
{
    char tmp[50]="";
    NANO_ENTRY* nan=&NanoTable->nano[SendDlgItemMessageA(hwndDlg, IDC_COMBO_CURNANO, CB_GETCURSEL, 0, 0)];
    sprintf(tmp, "%.8X", nan->addr);
    SetDlgItemTextA(hwndDlg, IDC_EDT_ADDR, tmp);
    sprintf(tmp, "%.8X", nan->dest);
    SetDlgItemTextA(hwndDlg, IDC_EDT_DEST, tmp);
    CheckDlgButton(hwndDlg, IDC_CHK_LOCKED, nan->locked);
    CheckDlgButton(hwndDlg, IDC_CHK_MARKER, nan->marker);
    CheckDlgButton(hwndDlg, IDC_CHK_FIXED, nan->fixed);
    bool longjmp=false;
    if(nan->size==6)
        longjmp=true;
    CheckDlgButton(hwndDlg, IDC_CHK_LONG, longjmp);
    bool enlongjmp=true;
    if(nan->type==Nano_JMP)
        enlongjmp=false;
    EnableWindow(GetDlgItem(hwndDlg, IDC_CHK_LONG), enlongjmp);
    for(int i=0,j=0; i<6; i++)
        j+=sprintf(tmp+j, "%.2X", nan->originalbytes[i]);
    SetDlgItemTextA(hwndDlg, IDC_EDT_ORIGINALBYTES, tmp);
    int newsel=nan->type;
    //printf("newsel: %d\n", newsel);
    SendDlgItemMessageA(hwndDlg, IDC_COMBO_TYPE, CB_SETCURSEL, newsel, 0);
}

BOOL CALLBACK DlgEdit(HWND hwndDlg, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
    switch(uMsg)
    {
    case WM_INITDIALOG:
    {
        char name[256]="";
        SendDlgItemMessageA(hwndDlg, IDC_COMBO_CURNANO, CB_RESETCONTENT, 0, 0);
        for(int i=0; i<NanoTable->total; i++)
        {
            sprintf(name, "%.8d:%.8X->%.8X", i+1, NanoTable->nano[i].addr, NanoTable->nano[i].dest);
            SendDlgItemMessageA(hwndDlg, IDC_COMBO_CURNANO, CB_ADDSTRING, 0, (LPARAM)name);
        }
        SendDlgItemMessageA(hwndDlg, IDC_COMBO_CURNANO, CB_SETCURSEL, 0, 0);

        SendDlgItemMessageA(hwndDlg, IDC_COMBO_TYPE, CB_ADDSTRING, 0, (LPARAM)"???");
        SendDlgItemMessageA(hwndDlg, IDC_COMBO_TYPE, CB_ADDSTRING, 0, (LPARAM)"Fake");
        SendDlgItemMessageA(hwndDlg, IDC_COMBO_TYPE, CB_ADDSTRING, 0, (LPARAM)"JMP");
        SendDlgItemMessageA(hwndDlg, IDC_COMBO_TYPE, CB_ADDSTRING, 0, (LPARAM)"JB");
        SendDlgItemMessageA(hwndDlg, IDC_COMBO_TYPE, CB_ADDSTRING, 0, (LPARAM)"JNB");
        SendDlgItemMessageA(hwndDlg, IDC_COMBO_TYPE, CB_ADDSTRING, 0, (LPARAM)"JE");
        SendDlgItemMessageA(hwndDlg, IDC_COMBO_TYPE, CB_ADDSTRING, 0, (LPARAM)"JNZ");
        SendDlgItemMessageA(hwndDlg, IDC_COMBO_TYPE, CB_ADDSTRING, 0, (LPARAM)"JA");
        SendDlgItemMessageA(hwndDlg, IDC_COMBO_TYPE, CB_ADDSTRING, 0, (LPARAM)"JBE");
        SendDlgItemMessageA(hwndDlg, IDC_COMBO_TYPE, CB_ADDSTRING, 0, (LPARAM)"JP");
        SendDlgItemMessageA(hwndDlg, IDC_COMBO_TYPE, CB_ADDSTRING, 0, (LPARAM)"JNP");
        SendDlgItemMessageA(hwndDlg, IDC_COMBO_TYPE, CB_ADDSTRING, 0, (LPARAM)"JL");
        SendDlgItemMessageA(hwndDlg, IDC_COMBO_TYPE, CB_ADDSTRING, 0, (LPARAM)"JGE");
        SendDlgItemMessageA(hwndDlg, IDC_COMBO_TYPE, CB_ADDSTRING, 0, (LPARAM)"JG");
        SendDlgItemMessageA(hwndDlg, IDC_COMBO_TYPE, CB_ADDSTRING, 0, (LPARAM)"JLE");
        SendDlgItemMessageA(hwndDlg, IDC_COMBO_TYPE, CB_ADDSTRING, 0, (LPARAM)"JS");
        SendDlgItemMessageA(hwndDlg, IDC_COMBO_TYPE, CB_ADDSTRING, 0, (LPARAM)"JNS");
        SendDlgItemMessageA(hwndDlg, IDC_COMBO_TYPE, CB_ADDSTRING, 0, (LPARAM)"JO");
        SendDlgItemMessageA(hwndDlg, IDC_COMBO_TYPE, CB_ADDSTRING, 0, (LPARAM)"JNO");
        SendDlgItemMessageA(hwndDlg, IDC_COMBO_TYPE, CB_ADDSTRING, 0, (LPARAM)"JCXZ");

        EditSelChange(hwndDlg);
    }
    return TRUE;

    case WM_COMMAND:
    {
        switch(LOWORD(wParam))
        {
        case IDC_BTN_CLOSE:
        {
            SendMessageA(hwndDlg, WM_CLOSE, 0, 0);
        }
        return TRUE;

        case IDC_COMBO_CURNANO:
        {
            switch(HIWORD(wParam))
            {
            case CBN_SELCHANGE:
                EditSelChange(hwndDlg);
                break;
            }
        }
        return TRUE;

        case IDC_BTN_GOTO:
        {
            int ret=SendDlgItemMessageA(hwndDlg, IDC_COMBO_CURNANO, CB_GETCURSEL, 0, 0);
            EndDialog(hwndDlg, ret);
        }
        return TRUE;

        case IDC_BTN_SEARCH:
        {
            char tmp[20]="";
            GetDlgItemTextA(hwndDlg, IDC_EDT_FIND, tmp, 20);
            unsigned int addr=0;
            sscanf(tmp, "%X", &addr);
            int found=-1;
            for(int i=0; i<NanoTable->total; i++)
                if(NanoTable->nano[i].addr==addr or NanoTable->nano[i].dest==addr)
                {
                    found=i;
                    break;
                }
            if(found!=-1)
            {
                SendDlgItemMessageA(hwndDlg, IDC_COMBO_CURNANO, CB_SETCURSEL, found, 0);
                EditSelChange(hwndDlg);
            }
            else
                MessageBoxA(hwndDlg, "Address/Destination not found!", "Error", MB_ICONERROR);
        }
        return TRUE;
        }
        return TRUE;
    }
    return TRUE;

    case WM_CLOSE:
    {
        EndDialog(hwndDlg, -1);
    }
    return TRUE;
    }
    return FALSE;
}

DWORD WINAPI AutoHandle(void* a)
{
    HWND hwndDlg=(HWND)a;
    curnano=0;
    EnableWindow(GetDlgItem(hwndDlg, IDC_BTN_AUTO), 0);
    while((NanoTable->nano[curnano].type==Nano_Fake or NanoTable->nano[curnano].locked) and curnano<NanoTable->total)
        curnano++;
    while(curnano<=NanoTable->total)
    {
        unsigned int addr=NanoTable->nano[curnano].addr;
        if(ContainsAddr(SilentDisasm(codepage, codestart, 0, codesize, addr-codestart), addr, 31) and !NanoTable->nano[curnano].fixed)
        {
            unsigned int rva=addr-codestart;
            unsigned char dest[6];
            int size=AssembleJump(dest, &NanoTable->nano[curnano], true);
            if(!size)
                continue;
            bool found=false;
            for(int i=1; i<size; i++)
                if(FindReference(addr+i))
                {
                    found=true;
                    break;
                }
            if((codepage[rva+1]==0xFF and codepage[rva+2]==0x25) or found)
            {
                RichDisasm(GetDlgItem(hwndDlg, IDC_EDT_DISASM), codepage, codestart, 0, codesize, addr-codestart);
                int res=MessageBoxA(hwndDlg, "This nanomite is disassembled, but it's suspected to be fake (due to found references), do you want to fix it anyway?\n\nClick 'Cancel' to stop Auto Mode here.", "Question", MB_ICONQUESTION|MB_YESNOCANCEL);
                if(res==IDYES)
                    ToggleNano(codepage, codestart, codesize, curnano, markersonly);
                else if(res==IDCANCEL)
                {
                    EnableWindow(GetDlgItem(hwndDlg, IDC_BTN_AUTO), 1);
                    SendMessageA(hwndDlg, WM_COMMAND, IDC_BTN_INIT, 0);
                    return 0;
                }

            }
            else
                ToggleNano(codepage, codestart, codesize, curnano, markersonly);
        }
        curnano++;
        while((NanoTable->nano[curnano].type==Nano_Fake or NanoTable->nano[curnano].locked) and curnano<NanoTable->total)
            curnano++;
    }
    EnableWindow(GetDlgItem(hwndDlg, IDC_BTN_AUTO), 1);
    SendMessageA(hwndDlg, WM_COMMAND, IDC_BTN_INIT, 0);
    MessageBoxA(hwndDlg, "Nanomites handled automatically, don't expect much, this tool was made for manual work anyway...", "...?", MB_ICONINFORMATION);
    return 0;
}

BOOL CALLBACK DlgMain(HWND hwndDlg, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
    switch(uMsg)
    {
    case WM_INITDIALOG:
    {
        SendMessageA(hwndDlg, WM_SETICON, ICON_BIG, (LPARAM)LoadIconA(hInst, MAKEINTRESOURCEA(IDI_ICON1)));
        SetLowerRightCorner(hwndDlg);
        SetForegroundWindow(hwndDlg);
        SendDlgItemMessageA(hwndDlg, IDC_EDT_DISASM, EM_SETBKGNDCOLOR, 0, RGB(255,251,240));

        shared=hwndDlg;
        HWND temp=GetDlgItem(hwndDlg, IDC_EDT_DUMPED);
        SetWindowLongA(temp, GWL_USERDATA, SetWindowLongA(temp, GWL_WNDPROC, (LONG)DropSubclass));
        SetWindowTextA(temp, "Drag & Drop your dumped file here...");
        temp=GetDlgItem(hwndDlg, IDC_EDT_PACKED);
        SetWindowLongA(temp, GWL_USERDATA, SetWindowLongA(temp, GWL_WNDPROC, (LONG)DropSubclass));
        SetWindowTextA(temp, "Drag & Drop your packed file here...");
        temp=GetDlgItem(hwndDlg, IDC_EDT_DISASM);
        SetWindowLongA(temp, GWL_USERDATA, SetWindowLongA(temp, GWL_WNDPROC, (LONG)DropSubclass));

        SetDlgItemTextA(hwndDlg, IDC_EDT_CODESTART, "00401000");

        RichDisasm(GetDlgItem(hwndDlg, IDC_EDT_DISASM), (unsigned char*)0x00400000, 0x00400000, 0, 0x2000, 0x1110);

        SetTimer(hwndDlg, 1, 100, 0);
    }
    return TRUE;

    case WM_TIMER:
    {
        if(GetForegroundWindow()!=hwndDlg)
            return TRUE;
        if(GetAsyncKeyState(VK_F2)) //toggle
        {
            if(GetTickCount()-lastpress[0]<300)
                return TRUE;
            SendMessageA(hwndDlg, WM_COMMAND, IDC_BTN_TOGGLE, 0);
            lastpress[0]=GetTickCount();
        }
        else if(GetAsyncKeyState(VK_F3)) //previous step
        {
            if(GetTickCount()-lastpress[1]<300)
                return TRUE;
            SendMessageA(hwndDlg, WM_COMMAND, IDC_BTN_STEPPREV, 0);
            lastpress[1]=GetTickCount();
        }
        else if(GetAsyncKeyState(VK_F4)) //previous
        {
            if(GetTickCount()-lastpress[2]<300)
                return TRUE;
            SendMessageA(hwndDlg, WM_COMMAND, IDC_BTN_PREV, 0);
            lastpress[2]=GetTickCount();
        }
        else if(GetAsyncKeyState(VK_F5)) //next
        {
            if(GetTickCount()-lastpress[3]<300)
                return TRUE;
            SendMessageA(hwndDlg, WM_COMMAND, IDC_BTN_NEXT, 0);
            lastpress[3]=GetTickCount();
        }
        else if(GetAsyncKeyState(VK_F6)) //find references
        {
            if(GetTickCount()-lastpress[4]<300)
                return TRUE;
            if(!NanoTable or !NanoTable->nano or !NanoTable->nano[curnano].marker)
                return TRUE;
            bool found=FindReference(NanoTable->nano[curnano].addr+2);
            found|=FindReference(NanoTable->nano[curnano].addr+3);
            found|=FindReference(NanoTable->nano[curnano].addr+4);
            if(found)
                MessageBoxA(hwndDlg, "Found at least one reference!", "Info", MB_ICONINFORMATION);
            else
                MessageBoxA(hwndDlg, "No references found...", "Info", MB_ICONINFORMATION);
            lastpress[4]=GetTickCount();
        }
        else if(GetAsyncKeyState(VK_F12)) //marker
        {
            if(GetTickCount()-lastpress[5]<300)
                return TRUE;
            SendMessageA(hwndDlg, WM_COMMAND, IDC_BTN_MARKER, 0);
            lastpress[5]=GetTickCount();
        }
    }
    return TRUE;

    case WM_CTLCOLORSTATIC:
    {
        if(GetDlgCtrlID((HWND)lParam)==IDC_STC_HELP)
        {
            SetTextColor((HDC)wParam,RGB(255,0,0));
            SetBkMode((HDC)wParam,OPAQUE);
            return (BOOL)br;
        }
    }
    break;

    case WM_CLOSE:
    {
        EndDialog(hwndDlg, 0);
    }
    return TRUE;

    case WM_COMMAND:
    {
        switch(LOWORD(wParam))
        {
        case IDC_BTN_SAVE:
        {
            if(!NanoTable)
            {
                MessageBoxA(hwndDlg, "Nothing to save", "Error", MB_ICONERROR);
                return TRUE;
            }
            char filename[256]="";
            if(!BrowseFileSave(hwndDlg, "Nanomite Logs (*.nan)\0*.nan", "nan", filename, sizeof(filename), szCurrentDir))
                return TRUE;
            unsigned char* data=(unsigned char*)emalloc(sizeof(NANO_TABLE)+sizeof(NANO_ENTRY)*NanoTable->total);
            if(!data)
            {
                MessageBoxA(hwndDlg, "malloc", "error", MB_ICONERROR);
                return TRUE;
            }
            unsigned int size=sizeof(NANO_TABLE)-sizeof(NANO_ENTRY*);
            memcpy(data, NanoTable, size);
            unsigned int totalsize=sizeof(NANO_ENTRY)*NanoTable->total;
            memcpy(data+size, NanoTable->nano, totalsize);
            totalsize+=size;
            HANDLE hFile=CreateFileA(filename, GENERIC_WRITE, 0, 0, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, 0);
            if(hFile==INVALID_HANDLE_VALUE)
            {
                efree(data);
                MessageBoxA(hwndDlg, "CreateFile", "error", MB_ICONERROR);
                return TRUE;
            }
            SetFilePointer(hFile, 0, 0, FILE_BEGIN);
            SetEndOfFile(hFile);
            DWORD written;
            if(!WriteFile(hFile, data, totalsize, &written, 0))
            {
                CloseHandle(hFile);
                efree(data);
                MessageBoxA(hwndDlg, "WriteFile", "error", MB_ICONERROR);
                return TRUE;
            }
            CloseHandle(hFile);
            efree(data);
            MessageBoxA(hwndDlg, "File saved!", "Ok", MB_ICONINFORMATION);
        }
        return TRUE;

        case IDC_BTN_AUTO:
        {
            if(NanoTable and NanoTable->nano and NanoTable->markersfixed)
                CreateThread(0, 0, AutoHandle, hwndDlg, 0, 0);
        }
        return TRUE;

        case IDC_BTN_FIXDUMP:
        {
            if(!dropped_dump)
            {
                MessageBoxA(hwndDlg, "No dump loaded", "Error", MB_ICONERROR);
                return TRUE;
            }
            FILEINFO* file=(FILEINFO*)emalloc(sizeof(FILEINFO));
            if(!file)
            {
                MessageBoxA(hwndDlg, "malloc", "error", MB_ICONERROR);
                return TRUE;
            }
            memset(file, 0, sizeof(FILEINFO));
            strcpy(file->FilePath, szDumpedName);
            ReadInFile(file);
            FillFileData(file);
            unsigned int imagebase=file->dwImageBase;
            unsigned int codestartraw=0;
            unsigned int codesizeraw=0;
            for(unsigned int i=0; i<file->dwNumberOfSections; i++)
                if(imagebase+file->psh[i].VirtualAddress==codestart)
                {
                    codestartraw=file->psh[i].PointerToRawData;
                    codesizeraw=file->psh[i].SizeOfRawData;
                }
            char fixedfile[256]="";
            BrowseFileSave(hwndDlg, "Executable Files (*.exe)\0*.exe\0\0", "exe", fixedfile, 256, szCurrentDir);
            HANDLE hFile=CreateFileA(fixedfile, GENERIC_WRITE, FILE_SHARE_READ, 0, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, 0);
            if(hFile==INVALID_HANDLE_VALUE)
            {
                FreeFileInfoStruct(file);
                MessageBoxA(hwndDlg, "CreateFileA", "error", MB_ICONERROR);
                return TRUE;
            }
            DWORD written=0;
            if(!WriteFile(hFile, file->lpFile, file->dwFileSize, &written, 0))
            {
                CloseHandle(hFile);
                FreeFileInfoStruct(file);
                MessageBoxA(hwndDlg, "WriteFile", "error", MB_ICONERROR);
                return TRUE;
            }
            SetFilePointer(hFile, codestartraw, 0, FILE_BEGIN);
            written=0;
            if(!WriteFile(hFile, codepage, codesizeraw, &written, 0))
            {
                CloseHandle(hFile);
                FreeFileInfoStruct(file);
                MessageBoxA(hwndDlg, "WriteFile", "error", MB_ICONERROR);
                return TRUE;
            }
            CloseHandle(hFile);
            FreeFileInfoStruct(file);
            MessageBoxA(hwndDlg, "Fixed dump saved!", "ok", MB_ICONINFORMATION);
        }
        return TRUE;

        case IDC_BTN_LOAD:
        {
            CheckDlgButton(hwndDlg, IDC_CHK_ONTOP, 0);
            SendMessageA(hwndDlg, WM_COMMAND, IDC_CHK_ONTOP, (LPARAM)GetDlgItem(hwndDlg, IDC_CHK_ONTOP));
            char filename[256]="";
            if(!BrowseFileOpen(hwndDlg, "Nanomite Logs (*.nan)\0*.nan", "nan", filename, sizeof(filename), szCurrentDir))
                return TRUE;
            HANDLE hFile=CreateFileA(filename, GENERIC_READ, FILE_SHARE_READ, 0, OPEN_EXISTING, 0, 0);
            if(hFile==INVALID_HANDLE_VALUE)
            {
                MessageBoxA(hwndDlg, "CreateFile", "error", MB_ICONERROR);
                return TRUE;
            }
            unsigned int filesize=GetFileSize(hFile, 0);
            unsigned char* data=(unsigned char*)emalloc(filesize);
            if(!data)
            {
                CloseHandle(hFile);
                MessageBoxA(hwndDlg, "malloc", "error", MB_ICONERROR);
                return TRUE;
            }
            DWORD read=0;
            if(!ReadFile(hFile, data, filesize, &read, 0))
            {
                free(data);
                CloseHandle(hFile);
                MessageBoxA(hwndDlg, "ReadFile", "error", MB_ICONERROR);
                return TRUE;
            }
            CloseHandle(hFile);
            unsigned int good_size=sizeof(NANO_TABLE)-sizeof(NANO_ENTRY*);
            NANO_TABLE* NewTable=(NANO_TABLE*)data;
            if(good_size+NewTable->total*sizeof(NANO_ENTRY)!=filesize)
            {
                efree(data);
                MessageBoxA(hwndDlg, "Invalid file format!", "error", MB_ICONERROR);
                return TRUE;
            }
            unsigned char* nano=(unsigned char*)emalloc(sizeof(NANO_ENTRY)*NewTable->total);
            if(!nano)
            {
                efree(data);
                MessageBoxA(hwndDlg, "malloc", "error", MB_ICONERROR);
                return TRUE;
            }
            memcpy(nano, data+good_size, NewTable->total*sizeof(NANO_ENTRY));
            NewTable->nano=(NANO_ENTRY*)nano;
            SetNanoTable(NewTable);
            efree(data);
            SendMessageA(hwndDlg, WM_COMMAND, IDC_BTN_INIT, 1);
        }
        return TRUE;

        case IDC_BTN_EDIT: //edit
        {
            if(!NanoTable or !NanoTable->nano)
                return TRUE;
            int ret=DialogBox(0, MAKEINTRESOURCE(DLG_EDIT), hwndDlg, (DLGPROC)DlgEdit);
            if(ret!=-1)
            {
                curnano=ret-1;
                SendMessageA(hwndDlg, WM_COMMAND, IDC_BTN_NEXT, 0);
            }
        }
        return TRUE;

        case IDC_BTN_INIT: //init
        {
            NanoTable=GetNanoTable();
            if(!NanoTable)
            {
                MessageBoxA(hwndDlg, "no table", "error", MB_ICONERROR);
                return TRUE;
            }
            codestart=NanoTable->codepage;
            char temp[10]="";
            sprintf(temp, "%.8X", codestart);
            SetDlgItemTextA(hwndDlg, IDC_EDT_CODESTART, temp);
            curnano=0;
            if(!NanoTable->markersfixed)
                SendMessageA(hwndDlg, WM_COMMAND, IDC_BTN_MARKER, 0);
            if(!markersonly)
            {
                while((NanoTable->nano[curnano].type==Nano_Fake or NanoTable->nano[curnano].locked) and curnano<NanoTable->total)
                    curnano++;
            }
            else //markers only
                while((!NanoTable->nano[curnano].marker or NanoTable->nano[curnano].locked) and curnano<NanoTable->total)
                    curnano++;
            if(curnano>=NanoTable->total)
            {
                if(markersonly)
                {
                    if(NanoTable->markersfixed) //to prevent deadlock when there were no nanomites found
                    {
                        SetWindowTextA(hwndDlg, "NanoLol");
                        markersonly=false;
                    }
                    else if(markersonly)
                    {
                        SetWindowTextA(hwndDlg, "NanoLol");
                        markersonly=false;
                    }
                    else
                    {
                        SetWindowTextA(hwndDlg, "NanoLol [Marker Mode]");
                        markersonly=true;
                    }
                    MessageBoxA(hwndDlg, "No valid/enabled markers found", "Error", MB_ICONERROR);
                    SendMessageA(hwndDlg, WM_COMMAND, IDC_BTN_MARKER, 0);
                    SendMessageA(hwndDlg, WM_COMMAND, IDC_BTN_INIT, 0);
                }
                else
                    MessageBoxA(hwndDlg, "No valid/enabled nanomites found", "Error", MB_ICONERROR);
                return TRUE;
            }
            firstnano=curnano;
            codepage=ReadCodePage(szDumpedName, codestart, &codesize);
            if(!codepage)
            {
                MessageBoxA(hwndDlg, "ReadCodePage", "error", MB_ICONERROR);
                return TRUE;
            }
            ProcessTable(codepage, codestart);
            if(lParam==1)
            {
                if(MessageBoxA(hwndDlg, "The program will now build a jump/call reference table, this can take a while.\n\nYes for normal reference table (recommended)\nNo for full reference table (experimental)", "Info", MB_ICONINFORMATION|MB_YESNO)==IDYES)
                {
                    if(!BuildReferenceTable(NanoTable, codepage, codesize, codestart))
                        MessageBoxA(hwndDlg, "Could not build reference table! (not enough memory?)", "error", MB_ICONERROR);
                }
                else
                {
                    if(!BuildFullReferenceTable(NanoTable, codepage, codesize, codestart))
                        MessageBoxA(hwndDlg, "Could not build reference table! (not enough memory?)", "error", MB_ICONERROR);
                }
            }
            SendMessageA(hwndDlg, WM_COMMAND, IDC_BTN_DISASM, 0);
        }
        return TRUE;

        case IDC_BTN_DISASM: //disasm
        {
            if(!NanoTable)
                return TRUE;
            TrackProgress(hwndDlg);
            char tmp[50]="";
            if(!markersonly)
                sprintf(tmp, "%d:%.8X:%d", curnano+1, NanoTable->nano[curnano].addr, NanoTable->nano[curnano].fixed);
            else
                sprintf(tmp, "MARKER:%d:%.8X:%d", curnano+1, NanoTable->nano[curnano].addr, NanoTable->nano[curnano].fixed);
            SetDlgItemTextA(hwndDlg, IDC_STC_INFO, tmp);
            unsigned int addr=NanoTable->nano[curnano].addr;
            unsigned int rva=addr-codestart;
            bool found=false;
            if(NanoTable->nano[curnano].marker)
            {
                found=FindReference(NanoTable->nano[curnano].addr+2);
                found|=FindReference(NanoTable->nano[curnano].addr+3);
                found|=FindReference(NanoTable->nano[curnano].addr+4);
                if(found)
                    SetDlgItemTextA(hwndDlg, IDC_STC_HELP, "Found reference(s) to marker!");
                else
                    SetDlgItemTextA(hwndDlg, IDC_STC_HELP, "Possibly a marker!");
            }
            else
                SetDlgItemTextA(hwndDlg, IDC_STC_HELP, "");
            unsigned int* addrs=RichDisasm(GetDlgItem(hwndDlg, IDC_EDT_DISASM), codepage, codestart, 0, codesize, rva);
            notdisassembled=false;
            if(!ContainsAddr(addrs, NanoTable->nano[curnano].addr, 31))
            {
                notdisassembled=true;
                if(!found)
                    SetDlgItemTextA(hwndDlg, IDC_STC_HELP, "INT3 not disassembled!");
            }
        }
        return TRUE;

        case IDC_BTN_TOGGLE:
        {
            if(!NanoTable)
                return TRUE;
            bool found=false;
            if(markersonly and NanoTable->nano[curnano].marker)
            {
                found=FindReference(NanoTable->nano[curnano].addr+2);
                found|=FindReference(NanoTable->nano[curnano].addr+3);
                found|=FindReference(NanoTable->nano[curnano].addr+4);
            }
            if(!NanoTable->nano[curnano].fixed and found)
            {
                if(MessageBoxA(hwndDlg, "Found reference(s) to bytes after the marker, this could indicate that the marker is fake, continue?", "Question", MB_YESNO)==IDYES)
                    ToggleNano(codepage, codestart, codesize, curnano, markersonly);
            }
            else
                ToggleNano(codepage, codestart, codesize, curnano, markersonly);
            SendMessageA(hwndDlg, WM_COMMAND, IDC_BTN_DISASM, 0);
        }
        return TRUE;

        case IDC_BTN_KILLFAKE:
        {
            KillFake(hwndDlg);
            SendMessageA(hwndDlg, WM_COMMAND, IDC_BTN_MARKER, 0);
            SendMessageA(hwndDlg, WM_COMMAND, IDC_BTN_INIT, 0);
        }
        return TRUE;

        case IDC_BTN_MARKER:
        {
            if(NanoTable && NanoTable->markersfixed)
            {
                SetWindowTextA(hwndDlg, "NanoLol");
                SetDlgItemTextA(hwndDlg, LOWORD(wParam), "Marker Mode");
                markersonly=false;
            }
            else if(markersonly)
            {
                SetWindowTextA(hwndDlg, "NanoLol");
                SetDlgItemTextA(hwndDlg, LOWORD(wParam), "Marker Mode");
                markersonly=false;
            }
            else
            {
                SetWindowTextA(hwndDlg, "NanoLol [Marker Mode]");
                SetDlgItemTextA(hwndDlg, LOWORD(wParam), "Normal Mode");
                markersonly=true;
            }
        }
        return TRUE;

        case IDC_BTN_NEXT: //next
        {
            bool domarkersonly=markersonly;
            if(markersonly and !NanoTable->nano[curnano].marker)
                domarkersonly=false;
            if(!NanoTable)
                return TRUE;
            curnano++;
            if(curnano>=NanoTable->total)
                curnano--;
            if(!domarkersonly)
                while((NanoTable->nano[curnano].type==Nano_Fake or NanoTable->nano[curnano].locked) and curnano<NanoTable->total)
                    curnano++;
            else //markers only
                while((!NanoTable->nano[curnano].marker or NanoTable->nano[curnano].locked) and curnano<NanoTable->total)
                    curnano++;
            if(curnano>=NanoTable->total)
            {
                SendMessageA(hwndDlg, WM_COMMAND, IDC_BTN_PREV, 0);
                if(markersonly)
                {
                    if(MessageBoxA(hwndDlg, "Last marker has been reached, do you want to kill nanomites that are out of marker range?", "Question", MB_ICONQUESTION|MB_YESNO)==IDYES)
                        SendMessageA(hwndDlg, WM_COMMAND, IDC_BTN_KILLFAKE, 0);
                }
            }
            SendMessageA(hwndDlg, WM_COMMAND, IDC_BTN_DISASM, 0);
        }
        return TRUE;

        case IDC_BTN_STEPPREV: //previous step
        {
            if(!NanoTable)
                return TRUE;
            curnano--;
            if(curnano<0)
                curnano=firstnano;
            while((NanoTable->nano[curnano].type==Nano_Fake or NanoTable->nano[curnano].locked) and curnano!=0)
                curnano--;
            if(curnano<=0)
                curnano=firstnano;
            SendMessageA(hwndDlg, WM_COMMAND, IDC_BTN_DISASM, 0);
        }
        return TRUE;

        case IDC_BTN_PREV: //previous
        {
            if(!NanoTable)
                return TRUE;
            curnano--;
            if(curnano<0)
                curnano=firstnano;
            if(!markersonly)
                while((NanoTable->nano[curnano].type==Nano_Fake or NanoTable->nano[curnano].locked) and curnano!=0)
                    curnano--;
            else //markers only
                while((!NanoTable->nano[curnano].marker or NanoTable->nano[curnano].locked) and curnano!=0)
                    curnano--;
            if(curnano<=0)
                curnano=firstnano;
            SendMessageA(hwndDlg, WM_COMMAND, IDC_BTN_DISASM, 0);
        }
        return TRUE;

        case IDC_BTN_GO:
        {
            SetFocus(0);
            CheckDlgButton(hwndDlg, IDC_CHK_ONTOP, 0);
            SendMessageA(hwndDlg, WM_COMMAND, IDC_CHK_ONTOP, (LPARAM)GetDlgItem(hwndDlg, IDC_CHK_ONTOP));
            unsigned int start=0;
            char temp[10]="";
            GetDlgItemTextA(hwndDlg, IDC_EDT_CODESTART, temp, 10);
            sscanf(temp, "%X", &start);
            if(!start or !dropped_dump or !dropped_packed)
            {
                MessageBoxA(hwndDlg, "Please fill everything in correctly...", "error", MB_ICONERROR);
                return TRUE;
            }
            if(!PopulateNanomites(szDumpedName, start))
            {
                MessageBoxA(hwndDlg, "Could not populate/find nanomites...", "error", MB_ICONERROR);
                return TRUE;
            }
            CreateThread(0, 0, DebugThread, 0, 0, 0);
        }
        return TRUE;

        case IDC_BTN_ABOUT:
        {
            MessageBoxA(hwndDlg, "NanoLol v0.1:\nCreated by Mr. eXoDia\nBeaEngine by BeatriX\nNanomite engine by Admiral\nTitanEngine by ReversingLabs\n\nThanks a lot!", "About", MB_ICONINFORMATION);
        }
        return TRUE;

        case IDC_CHK_ONTOP:
        {
            SetFocus(0);
            HWND flag=HWND_NOTOPMOST;
            if(IsDlgButtonChecked(hwndDlg, LOWORD(wParam)))
                flag=HWND_TOPMOST;
            SetWindowPos(hwndDlg, flag, 0, 0, 0, 0, SWP_NOMOVE|SWP_NOSIZE|SWP_SHOWWINDOW);
        }
        return TRUE;
        }
    }
    return TRUE;
    }
    return FALSE;
}

int APIENTRY WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nShowCmd)
{
    hInst=hInstance;
    InitCommonControls();
    LoadLibraryA("riched20.dll");
    return DialogBox(hInst, MAKEINTRESOURCE(DLG_MAIN), NULL, (DLGPROC)DlgMain);
}
