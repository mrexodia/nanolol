#ifndef _GLOBAL_H
#define _GLOBAL_H

#include <stdio.h>

void efree(void* ptr);
void* emalloc(size_t size);

#endif // _GLOBAL_H
