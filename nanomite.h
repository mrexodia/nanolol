#ifndef _NANOMITE_H
#define _NANOMITE_H

#include <windows.h>

enum JUMP_TYPE
{
    Nano_JUnknown = 0,
    Nano_Fake = 1,
    Nano_JMP = 2,
    //JA=JNBE
    Nano_JA = 3,
    //JNC=JNB=JAE
    Nano_JNB = 4,
    //JG=JNLE
    Nano_JG = 5,
    //JNL=JGE
    Nano_JGE = 6,
    //JC=JB=JNAE
    Nano_JB = 7,
    //JE=JZ
    Nano_JE = 8,
    //JNE=JNZ
    Nano_JNZ = 9,
    //JP=JPE
    Nano_JP = 10,
    //JNP=JPO
    Nano_JPO = 11,
    //JL=JNGE
    Nano_JL = 12,
    //others
    Nano_JS = 13,
    Nano_JNS = 14,
    Nano_JO = 15,
    Nano_JNO = 16,
    Nano_JCXZ = 17,
    //JNG=JLE
    Nano_JLE = 18,
    //JNA=JBE
    Nano_JBE = 19,
};

enum JFlag
{
    CX = 1,
    PF = 2,
    OF = 4,
    SF = 8,
    ZF = 16,
    CF = 32,
};

struct NANO_ENTRY
{
    unsigned int addr;
    unsigned int dest;
    int size;
    JUMP_TYPE type;
    unsigned char originalbytes[6];
    bool fixed;
    bool marker;
    bool locked;
};

struct NANO_TABLE
{
    int total;
    int fake;
    int markersfixed;
    unsigned int codepage;
    NANO_ENTRY* nano;
};

extern char szFileName[256];
extern char szDumpedName[256];
extern char szCurrentDir[256];

unsigned int AssembleJump(unsigned char* destination, NANO_ENTRY* nano, bool silent);
void ProcessTable(unsigned char* codepage, unsigned int codeva);
void KillFake(HWND hwndDlg);
void ToggleNano(unsigned char* codepage, unsigned int codestart, unsigned int codesize, int curnano, bool marker);
NANO_TABLE* GetNanoTable();
void SetNanoTable(NANO_TABLE* NewTable);
unsigned char* ReadCodePage(const char* file, unsigned int start, unsigned int* read);
bool PopulateNanomites(const char* file, unsigned int start);
DWORD WINAPI DebugThread(LPVOID lpStartAddress);

#endif // _NANOMITE_H
