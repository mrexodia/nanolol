#include <stdio.h>
#include <windows.h>
#include <commctrl.h>
#include "nanomite.h"
#include "debug_helper.h"
#include "TitanEngine\TitanEngine.h"
#include "resource.h"
#include "_global.h"

static long fdImageBase = NULL;
static long fdEntryPoint = NULL;
static long fdEntrySectionNumber = NULL;
static long fdEntrySectionSize = NULL;
static long fdEntrySectionOffset = NULL;

char szFileName[256]=""; //filepath for packed file
char szDumpedName[256]=""; //filepath for dumped file
char szCurrentDir[256]=""; //for saving files
char log_message[256]="";
static ULONG_PTR va;

//Relevant variables:
static bool started_nano=false; //Bool for checking if the debug event template was copied
static bool cde_patched=false; //Bool for patch on ContinueDebugEvent
static DEBUG_EVENT RemoteDE= {0}; //Remote debug event
static DEBUG_EVENT TemplateDE= {0}; //Template remote debug event
static int current_nanomite=0; //current nanomite table index
static int repeat_calls=0; //Consecutive calls to GetThreadContext
static CONTEXT ChildContext= {0}; //Child Context
static CONTEXT PreContext= {0}; //Pre Context
static CONTEXT PostContext= {0}; //Post Context
static unsigned int ConditionTable[64]= {0}; //Condition table
static int ConditionTableIndex=0; //Condition table index

static NANO_TABLE NanoTable;

NANO_TABLE* GetNanoTable()
{
    if(NanoTable.nano and NanoTable.total)
        return &NanoTable;
    return 0;
}

void SetNanoTable(NANO_TABLE* NewTable)
{
    if(NanoTable.nano)
        free(NanoTable.nano);
    memcpy(&NanoTable, NewTable, sizeof(NANO_TABLE));
}

static int GetLongJmpDw(unsigned int start, unsigned int dest)
{
    return dest-start-5;
}

static int GetLongDw(unsigned int start, unsigned int dest)
{
    return dest-start-6;
}

static int GetShortDw(unsigned int start, unsigned int dest)
{
    return dest-start-2;
}

static void TrackProgress(HWND hwndDlg)
{
    int pdone_int=((current_nanomite+1)*100)/NanoTable.total;
    SendDlgItemMessageA(hwndDlg, IDC_PROGRESS1, PBM_SETPOS, pdone_int, 0);
}

unsigned int AssembleJump(unsigned char* destination, NANO_ENTRY* nano, bool silent)
{
    if(nano->type==Nano_JUnknown or nano->type==Nano_Fake)
    {
        if(!silent)
            MessageBoxA(0, "Nano_JUnknown, please report to Mr. eXoDia (mr.exodia.tpodt@gmail.com)", "Please report", MB_ICONERROR|MB_SYSTEMMODAL);
        return 0;
    }
    int opcode_size=1;
    int dw_size=1;
    bool longjmp=false;
    if(nano->size==6 or nano->size==5)
    {
        longjmp=true;
        dw_size=4;
    }
    if(nano->size==6)
        opcode_size=2;

    if(longjmp and nano->type==Nano_JCXZ)
        return 0;

    unsigned char instr[7];
    unsigned int opcode=0;
    memset(instr, 0, sizeof(instr));
    switch(nano->type)
    {
    case Nano_JMP:
        if(longjmp)
            opcode=0xE9;
        else
            opcode=0xEB;
        break;
    case Nano_JNZ:
        if(longjmp)
            opcode=0x850F;
        else
            opcode=0x75;
        break;
    case Nano_JE:
        if(longjmp)
            opcode=0x840F;
        else
            opcode=0x74;
        break;
    case Nano_JB:
        if(longjmp)
            opcode=0x820F;
        else
            opcode=0x72;
        break;
    case Nano_JBE:
        if(longjmp)
            opcode=0x860F;
        else
            opcode=0x76;
        break;
    case Nano_JA:
        if(longjmp)
            opcode=0x870F;
        else
            opcode=0x77;
        break;
    case Nano_JNB:
        if(longjmp)
            opcode=0x830F;
        else
            opcode=0x73;
        break;
    case Nano_JG:
        if(longjmp)
            opcode=0x8F0F;
        else
            opcode=0x7F;
        break;
    case Nano_JGE:
        if(longjmp)
            opcode=0x8D0F;
        else
            opcode=0x7D;
        break;
    case Nano_JL:
        if(longjmp)
            opcode=0x8C0F;
        else
            opcode=0x7C;
        break;
    case Nano_JLE:
        if(longjmp)
            opcode=0x8E0F;
        else
            opcode=0x7E;
        break;
    case Nano_JP:
        if(longjmp)
            opcode=0x8A0F;
        else
            opcode=0x7A;
        break;
    case Nano_JPO:
        if(longjmp)
            opcode=0x8B0F;
        else
            opcode=0x7B;
        break;
    case Nano_JS:
        if(longjmp)
            opcode=0x880F;
        else
            opcode=0x78;
        break;
    case Nano_JNS:
        if(longjmp)
            opcode=0x890F;
        else
            opcode=0x79;
        break;
    case Nano_JCXZ:
        opcode=0xE3;
        break;
    case Nano_JO:
        if(longjmp)
            opcode=0x800F;
        else
            opcode=0x70;
        break;
    case Nano_JNO:
        if(longjmp)
            opcode=0x810F;
        else
            opcode=0x71;
        break;
    default:
        return 0;
    }
    int dw;
    if(longjmp and opcode_size==1)
        dw=GetLongJmpDw(nano->addr, nano->dest);
    else if(longjmp)
        dw=GetLongDw(nano->addr, nano->dest);
    else
        dw=GetShortDw(nano->addr, nano->dest);
    memcpy(instr, &opcode, opcode_size); //opcode
    memcpy(instr+opcode_size, &dw, dw_size);
    /*for(int i=0; i<opcode_size; i++)
        printf("%.2X", instr[i]);
    printf(" ");
    for(int i=0; i<dw_size; i++)
        printf("%.2X", instr[opcode_size+i]);
    puts("");*/
    memcpy(destination, instr, opcode_size+dw_size);
    return opcode_size+dw_size;
}

static int CleanupTable()
{
    int fake=0;
    for(int i=0; i<NanoTable.total; i++)
        if(NanoTable.nano[i].type==Nano_Fake)
            fake++;
    NanoTable.fake=fake;
    if(!NanoTable.fake or !NanoTable.nano)
        return 0;
    int real=NanoTable.total-NanoTable.fake;
    if(!real)
        return 0;
    NANO_ENTRY* nano=(NANO_ENTRY*)emalloc(real*sizeof(NANO_ENTRY));
    if(!nano)
        return 0;
    memset(nano, 0, real*sizeof(NANO_ENTRY));
    for(int i=0,j=0; i<NanoTable.total; i++)
        if(NanoTable.nano[i].type!=Nano_Fake)
        {
            memcpy(&nano[j], &NanoTable.nano[i], sizeof(NANO_ENTRY));
            j++;
        }
    free(NanoTable.nano);
    NanoTable.nano=nano;
    NanoTable.fake=0;
    NanoTable.total=real;
    return fake;
}

void ProcessTable(unsigned char* codepage, unsigned int codeva)
{
    for(int i=0; i<NanoTable.total; i++)
    {
        if(NanoTable.nano[i].type==Nano_Fake or NanoTable.nano[i].type==Nano_JUnknown)
            continue;
        unsigned int rva=NanoTable.nano[i].addr-codeva;
        if(NanoTable.nano[i].fixed)
        {
            unsigned int size=AssembleJump(codepage+rva, &NanoTable.nano[i], false);
            if(NanoTable.nano[i].marker and size==2)
                memset(codepage+rva+size, 0x90, 3);
        }
        else
            memcpy(codepage+rva, NanoTable.nano[i].originalbytes, 6);
    }
}

void KillFake(HWND hwndDlg)
{
    int marker_count=0;
    for(int i=0; i<NanoTable.total; i++)
        if(NanoTable.nano[i].marker and NanoTable.nano[i].fixed)
            marker_count++;
    if(!marker_count) //no markers
        return;
    if(marker_count%2)
    {
        MessageBoxA(hwndDlg, "Odd fixed marker number, please re-check all markers.", "Error", MB_ICONERROR);
        return;
    }
    //printf("marker count: %d\n", marker_count);
    //remove fake markers & set the locked property
    for(int i=0; i<NanoTable.total; i++)
        if(NanoTable.nano[i].marker)
        {
            if(!NanoTable.nano[i].fixed)
                NanoTable.nano[i].marker=false;
            else
                NanoTable.nano[i].locked=true;
        }
    bool remove=true;
    for(int i=0; i<NanoTable.total; i++)
    {
        if(NanoTable.nano[i].marker)
        {
            if(remove)
                remove=false;
            else
                remove=true;
            continue;
        }
        if(remove)
        {
            NanoTable.nano[i].type=Nano_Fake;
            NanoTable.fake++;
        }
    }
    NanoTable.markersfixed=1;
    CleanupTable();
    MessageBoxA(hwndDlg, "All nanomites outside of NANOSTART/NANOEND blocks have been removed.", "Info", MB_ICONINFORMATION);
}

void ToggleNano(unsigned char* codepage, unsigned int codestart, unsigned int codesize, int curnano, bool marker)
{
    if(NanoTable.nano[curnano].locked)
        return;
    unsigned int addr=NanoTable.nano[curnano].addr;
    unsigned int rva=addr-codestart;
    unsigned char dest[6];
    int size=AssembleJump(dest, &NanoTable.nano[curnano], false);
    if(!size)
        return;
    if(!memcmp(NanoTable.nano[curnano].originalbytes, codepage+rva, size)) //not fixed yet
    {
        memcpy(codepage+rva, dest, size);
        if(NanoTable.nano[curnano].marker and marker and size==2)
            memset(codepage+rva+size, 0x90, 3);
        NanoTable.nano[curnano].fixed=true;
    }
    else //restore original bytes
    {
        if(NanoTable.nano[curnano].marker and size==2)
            size+=3;
        memcpy(codepage+rva, NanoTable.nano[curnano].originalbytes, size);
        NanoTable.nano[curnano].fixed=false;
    }
}

unsigned char* ReadCodePage(const char* file, unsigned int start, unsigned int* read)
{
    STARTUPINFO si;
    PROCESS_INFORMATION pi;
    char currentdir[256]="";
    strcpy(currentdir, file);
    int i=strlen(currentdir);
    while(file[i]!='\\')
        i--;
    currentdir[i]=0;
    ZeroMemory(&si, sizeof(si));
    si.cb=sizeof(si);
    ZeroMemory(&pi, sizeof(pi));
    if(!CreateProcess(file, 0, 0, 0, FALSE, CREATE_SUSPENDED|CREATE_NO_WINDOW, 0, currentdir, &si, &pi))
        return false;
    MEMORY_BASIC_INFORMATION mbi;
    memset(&mbi, 0, sizeof(MEMORY_BASIC_INFORMATION));
    VirtualQueryEx(pi.hProcess, (void*)start, &mbi, sizeof(MEMORY_BASIC_INFORMATION));
    unsigned int size=mbi.RegionSize;
    if(read)
        *read=size;
    BYTE* memory_dump=(BYTE*)emalloc(size);
    if(!ReadProcessMemory(pi.hProcess, (void*)start, memory_dump, size, 0))
    {
        free(memory_dump);
        return 0;
    }
    TerminateProcess(pi.hProcess, 0);
    CloseHandle(pi.hProcess);
    CloseHandle(pi.hThread);
    return memory_dump;
}

bool PopulateNanomites(const char* file, unsigned int start)
{
    //Clear vars
    started_nano=false;
    cde_patched=false;
    repeat_calls=0;
    ConditionTableIndex=0;
    //Populate nanomites
    unsigned int size=0;
    unsigned char* memory_dump=ReadCodePage(file, start, &size);
    if(!memory_dump)
        return false;
    int nano_count=0;
    for(unsigned int i=0; i<size; i++)
        if(memory_dump[i]==0xCC) //CC=int3
            nano_count++;
    if(NanoTable.nano)
        free(NanoTable.nano);
    memset(&NanoTable, 0, sizeof(NANO_TABLE));
    NanoTable.nano=(NANO_ENTRY*)emalloc(nano_count*sizeof(NANO_ENTRY));
    memset(NanoTable.nano, 0, nano_count*sizeof(NANO_ENTRY));
    NanoTable.total=nano_count;
    NanoTable.codepage=start;
    int current_nanomite=0;
    for(unsigned int i=0; i<size; i++)
    {
        if(memory_dump[i]==0xCC)
        {
            NanoTable.nano[current_nanomite].addr=i+start;
            memcpy(NanoTable.nano[current_nanomite].originalbytes, memory_dump+i, 6);
            current_nanomite++;
            if(!(current_nanomite%10))
                TrackProgress(shared);
            if(current_nanomite==nano_count)
            {
                current_nanomite=0;
                break;
            }
        }
    }
    free(memory_dump);
    if(nano_count)
        return true;
    else
        return false;
}

bool CompareJumpTable(JUMP_TYPE Type, unsigned int Destination) //This function checks whether all conditions pass different flag configurations (brute force)
{
    dbg("CompareJumpTable");
    bool Goes=false; //This must be true for any condition to pass the jump test
    for(int i=0; i<64; i++) //64 conditions in the table
    {
        BYTE fOF=0;
        BYTE fSF=0;
        BYTE fZF=0;
        BYTE fPF=0;
        BYTE fCF=0;
        BYTE fCX=1&(i&CX); //CX (low order word, Ecx=1 or Ecx=0)
        unsigned int EFlags=(0x800&-(int)((i&OF)!=0)); //OF (overflow flag)
        EFlags|=(0x80&-(int)((i&SF)!=0)); //SF (sign flag)
        EFlags|=(0x40&-(int)((i&ZF)!=0)); //ZF (zero flag)
        EFlags|=(0x4&-(int)((i&PF)!=0)); //PF (parity flag)
        EFlags|=(0x1&-(int)((i&CF)!=0)); //CF (carry flag)
        if(EFlags&0x800)
            fOF=1;
        if(EFlags&0x80)
            fSF=1;
        if(EFlags&0x40)
            fZF=1;
        if(EFlags&0x4)
            fPF=1;
        if(EFlags&0x1)
            fCF=1;
        switch(Type)
        {
        case Nano_JA:
            Goes=(fCF==0 and fZF==0);
            break;
        case Nano_JNB:
            Goes=(fCF==0);
            break;
        case Nano_JG:
            Goes=(fZF==0 and fSF==fOF);
            break;
        case Nano_JGE:
            Goes=(fSF==fOF);
            break;
        case Nano_JB:
            Goes=(fCF==1);
            break;
        case Nano_JE:
            Goes=(fZF==1);
            break;
        case Nano_JNZ:
            Goes=(fZF==0);
            break;
        case Nano_JP:
            Goes=(fPF==1);
            break;
        case Nano_JPO:
            Goes=(fPF==0);
            break;
        case Nano_JL:
            Goes=(fSF!=fOF);
            break;
        case Nano_JNS:
            Goes=(fSF==0);
            break;
        case Nano_JS:
            Goes=(fSF==1);
            break;
        case Nano_JCXZ:
            Goes=(fCX==0);
            break;
        case Nano_JO:
            Goes=(fOF==1);
            break;
        case Nano_JNO:
            Goes=(fOF==0);
            break;
        case Nano_JBE:
            Goes=(fCF==1 or fZF==1);
            break;
        case Nano_JLE:
            Goes=(fZF==1 or fSF!=fOF);
            break;
        default:
            return false;
        }
        if(Goes!=(ConditionTable[i]==Destination)) //Compare whether Goes passes the condition
            return false; //False if it does not pass
    }
    return true; //True if we never had a false condition
}

void IdentifyNano()
{
    dbg("IdentifyNano");
    //int Offset=0;

    bool IsJMP=false;
    bool Inconsistent=false;

    unsigned int dest1=0,dest2=0;
    dest2=dest1=ConditionTable[0];
    //get two destinations
    for(int i=1; i<64; i++)
    {
        if(ConditionTable[i]!=dest1)
        {
            dest2=ConditionTable[i];
            break;
        }
    }
    unsigned int addr=NanoTable.nano[current_nanomite].addr;
    dest1+=addr;
    dest2+=addr;
    int size=2;
    unsigned int real_dest;
    if(dest1==dest2) //no different destinations
    {
        real_dest=dest1;
        int diff=GetShortDw(addr, real_dest);
        if(diff<-128 or diff>127)
            size=5;
        IsJMP=true;
    }
    else
    {
        //check jump inconsistency
        for(int i=0; i<64; i++)
        {
            if(ConditionTable[i]!=dest1-addr and ConditionTable[i]!=dest2-addr)
            {
                MessageBoxA(shared, "Inconsistent jump, please report to Mr. eXoDia (mr.exodia.tpodt@gmail.com)", "error", MB_ICONERROR);
                Inconsistent=true;
                break;
            }
        }
        if(addr+2==dest1)
            real_dest=dest2;
        else if(addr+2==dest2)
            real_dest=dest1;
        else if(addr+6==dest1)
        {
            size=6;
            real_dest=dest2;
        }
        else if(addr+6==dest2)
        {
            real_dest=dest1;
            size=6;
        }

    }
    NanoTable.nano[current_nanomite].size=size;
    NanoTable.nano[current_nanomite].dest=real_dest;
    if(IsJMP)
    {
        NanoTable.nano[current_nanomite].type=Nano_JMP;
        return;
    }
    //Get jump type
    int found=0;
    for(int CompType=Nano_JA; CompType<=Nano_JBE; CompType++) //work through all condentional jump types
    {
        if(CompareJumpTable((JUMP_TYPE)CompType, real_dest-addr)) //IMPORTANT: Check if the current type matches the one in the offset table (check this function)
        {
            if(!found)
                NanoTable.nano[current_nanomite].type=(JUMP_TYPE)CompType;
            found++;
        }
    }
    if(found>1)// or !found)
    {
        char msg[256]="";
        sprintf(msg, "Found %d matches of %.8X->%.8X", found, addr, real_dest);
        MessageBoxA(shared, msg, "problem", MB_ICONERROR);
    }
}

void ComparePrePostContexts() //IMPORTANT: This function does the actual job
{
    ConditionTable[ConditionTableIndex]=(unsigned int)(PostContext.Eip-PreContext.Eip+1); //Set the difference of the Pre and Post context in the condition table (read: jump offset, sort of)
    ConditionTableIndex++; //We got another condition
    if(ConditionTableIndex>=64) //If we retrieved 64 conditions we compare all these conditions
    {
        ConditionTableIndex=0;
        IdentifyNano();
        /*for(int i=0; i<64; i++) //for debugging
            printf("%.8X\n", ConditionTable[i]+NanoTable.nano[current_nanomite].addr);*/
        current_nanomite++;
        if(!(current_nanomite%10))
            TrackProgress(shared);

        char jump_type[20]="???";
        switch(NanoTable.nano[current_nanomite-1].type)
        {
        case Nano_JUnknown:
            strcpy(jump_type, "JUnknown");
            break;
        case Nano_Fake:
            strcpy(jump_type, "Fake");
            break;
        case Nano_JMP:
            strcpy(jump_type, "JMP");
            break;
        case Nano_JNZ:
            strcpy(jump_type, "JNZ");
            break;
        case Nano_JE:
            strcpy(jump_type, "JE");
            break;
        case Nano_JB:
            strcpy(jump_type, "JB");
            break;
        case Nano_JBE:
            strcpy(jump_type, "JBE");
            break;
        case Nano_JA:
            strcpy(jump_type, "JA");
            break;
        case Nano_JNB:
            strcpy(jump_type, "JNB");
            break;
        case Nano_JG:
            strcpy(jump_type, "JG");
            break;
        case Nano_JGE:
            strcpy(jump_type, "JGE");
            break;
        case Nano_JL:
            strcpy(jump_type, "JL");
            break;
        case Nano_JLE:
            strcpy(jump_type, "JLE");
            break;
        case Nano_JP:
            strcpy(jump_type, "JP");
            break;
        case Nano_JPO:
            strcpy(jump_type, "JPO");
            break;
        case Nano_JS:
            strcpy(jump_type, "JS");
            break;
        case Nano_JNS:
            strcpy(jump_type, "JNS");
            break;
        case Nano_JCXZ:
            strcpy(jump_type, "JCXZ");
            break;
        case Nano_JO:
            strcpy(jump_type, "JO");
            break;
        case Nano_JNO:
            strcpy(jump_type, "JNO");
            break;
        }
        if(NanoTable.nano[current_nanomite-1].type==Nano_JMP and NanoTable.nano[current_nanomite-1].dest-NanoTable.nano[current_nanomite-1].addr==5)
            NanoTable.nano[current_nanomite-1].marker=true;
        //printf("Addr : %.8X, Dest : %.8X, Size : %.8X, Type : %s\n", NanoTable.nano[current_nanomite-1].addr, NanoTable.nano[current_nanomite-1].dest, NanoTable.nano[current_nanomite-1].size, jump_type); //for debugging
        if(current_nanomite>=NanoTable.total)
        {
            //printf("\nAll nanomites processed (Total: %d, 'Real': %d)\n", NanoTable.total, NanoTable.total-NanoTable.fake);
            StopDebug();
            return;
        }
    }
    return;
}

void cbSetThreadContext()
{
    repeat_calls=0;
    unsigned int ChildContext_ptr=0; //Pointer to context of child
    unsigned int esp=GetContextData(UE_ESP);
    rpm(esp+8, &ChildContext_ptr, 4); //Read dw ptr ds[esp+8]
    rpm(ChildContext_ptr, &PostContext, sizeof(CONTEXT)); //Read the child context
    ComparePrePostContexts();
    SetContextData(UE_EAX, 1);
}

void cbGetThreadContext()
{
    repeat_calls++;
    if(repeat_calls>1)
    {
        NanoTable.fake++;
        current_nanomite++;
        if(!(current_nanomite%10))
            TrackProgress(shared);
        ConditionTableIndex=0;
        NanoTable.nano[current_nanomite].type=Nano_Fake;
        if(current_nanomite>=NanoTable.total)
        {
            //printf("\nAll nanomites processed (Total: %d, 'Real': %d)\n", NanoTable.total, NanoTable.total-NanoTable.fake);
            StopDebug();
            return;
        }
    }
    unsigned int ChildContext_ptr=0; //Pointer to context of child
    unsigned int esp=GetContextData(UE_ESP);
    rpm(esp+8, &ChildContext_ptr, 4); //Read dw ptr ds[esp+8]
    rpm(ChildContext_ptr, &ChildContext, sizeof(CONTEXT)); //Read the child context
    //This is used to generate all possible flag combinations
    ChildContext.EFlags = 0; //Set the flags to 0
    ChildContext.Ecx=1&(ConditionTableIndex&CX); //CX (low order word, Ecx=1 or Ecx=0)
    ChildContext.EFlags|=(0x800&-(int)((ConditionTableIndex&OF)!=0)); //OF (overflow flag)
    ChildContext.EFlags|=(0x80&-(int)((ConditionTableIndex&SF)!=0)); //SF (sign flag)
    ChildContext.EFlags|=(0x40&-(int)((ConditionTableIndex&ZF)!=0)); //ZF (zero flag)
    ChildContext.EFlags|=(0x4&-(int)((ConditionTableIndex&PF)!=0)); //PF (parity flag)
    ChildContext.EFlags|=(0x1&-(int)((ConditionTableIndex&CF)!=0)); //CF (carry flag)
    ChildContext.Eip=(unsigned int)NanoTable.nano[current_nanomite].addr; //Set the EIP of the child context to a potential nano, so the father can process it for us
    ChildContext.Eip++; //Increase this eip (dont ask me why, probably this can be found out though)
    memcpy(&PreContext, &ChildContext, sizeof(CONTEXT)); //Copy this context in PreContext (for later use)
    wpm(ChildContext_ptr, &ChildContext, sizeof(CONTEXT));
    //not very useful, just because it's possible I guess
    //unsigned int return_addr=0;
    //rpm(esp, &return_addr, 4);
    SetContextData(UE_EAX, 1);
    //SetContextData(UE_ESP, esp+12);
    //SetContextData(UE_EIP, return_addr);
}

void cbWaitForDebugEvent()
{
    unsigned int remoteDE_ptr=0; //Remote Debug Event Pointer
    rpm(GetContextData(UE_ESP)+4, &remoteDE_ptr, 4); //Read the remote pointer
    if(started_nano)
    {
        if(!cde_patched)
        {
            cde_patched=true;
            BYTE cde_patch[]= {0x33, 0xC0, 0x40, 0xC2, 0x0C, 0x00};
            unsigned int ContinueDebugEvent_addr=(unsigned int)GetProcAddress(GetModuleHandleA("kernel32.dll"), "ContinueDebugEvent");
            wpm(ContinueDebugEvent_addr, &cde_patch, sizeof(cde_patch));
            SetAPIBreakPoint((char*)"kernel32.dll", (char*)"GetThreadContext", UE_BREAKPOINT, UE_APIEND, (void*)cbGetThreadContext);
            SetAPIBreakPoint((char*)"kernel32.dll", (char*)"SetThreadContext", UE_BREAKPOINT, UE_APIEND, (void*)cbSetThreadContext);
        }
        memcpy(&RemoteDE, &TemplateDE, sizeof(DEBUG_EVENT));

        RemoteDE.u.Exception.ExceptionRecord.ExceptionAddress=(void*)NanoTable.nano[current_nanomite].addr;
        wpm(remoteDE_ptr, &TemplateDE, sizeof(DEBUG_EVENT));
        unsigned int return_addr=0;
        unsigned int esp=GetContextData(UE_ESP);
        rpm(esp, &return_addr, 4);
        SetContextData(UE_EAX, 1);
        SetContextData(UE_ESP, esp+12);
        SetContextData(UE_EIP, return_addr);
    }
    rpm(remoteDE_ptr, &RemoteDE, sizeof(DEBUG_EVENT)); //Read the data
    if(RemoteDE.dwDebugEventCode==EXCEPTION_DEBUG_EVENT)
    {
        if(RemoteDE.u.Exception.ExceptionRecord.ExceptionCode==EXCEPTION_BREAKPOINT)
        {
            if((unsigned int)RemoteDE.u.Exception.ExceptionRecord.ExceptionAddress<0x70000000)
            {
                memcpy(&TemplateDE, &RemoteDE, sizeof(DEBUG_EVENT));
                unsigned int one=1;
                memcpy((BYTE*)(&TemplateDE)+0x50, &one, 4);
                started_nano=true;
            }
        }
    }
}

void cbDebugActiveProcess()
{
    DeleteAPIBreakPoint((char*)"kernel32.dll", (char*)"DebugActiveProcess", UE_APISTART);
    SetAPIBreakPoint((char*)"kernel32.dll", (char*)"WaitForDebugEvent", UE_BREAKPOINT, UE_APISTART, (void*)cbWaitForDebugEvent);
}

void cbEntry()
{
    FixIsDebuggerPresent(fdProcessInfo->hProcess, true);
    SetAPIBreakPoint((char*)"kernel32.dll", (char*)"DebugActiveProcess", UE_BREAKPOINT, UE_APISTART, (void*)cbDebugActiveProcess);
}

DWORD WINAPI DebugThread(LPVOID lpStartAddress)
{
    EnableWindow(GetDlgItem(shared, IDC_BTN_GO), 0);
    fdImageBase = NULL;
    fdEntryPoint = NULL;
    fdProcessInfo = NULL;
    FILE_STATUS_INFO inFileStatus = {0};
    SetEngineVariable(UE_ENGINE_NO_CONSOLE_WINDOW, true);
    if(IsPE32FileValidEx(szFileName, UE_DEPTH_DEEP, &inFileStatus))
    {
        HANDLE hFile, fileMap;
        fdImageBase = (long)GetPE32Data(szFileName, NULL, UE_IMAGEBASE);
        fdEntryPoint = (long)GetPE32Data(szFileName, NULL, UE_OEP);
        DWORD bytes_read;
        StaticFileLoad(szFileName, UE_ACCESS_READ, false, &hFile, &bytes_read, &fileMap, &va);
        fdEntrySectionNumber = GetPE32SectionNumberFromVA(va, fdEntryPoint+fdImageBase);
        StaticFileClose(hFile);
        fdEntrySectionSize= (long)GetPE32Data(szFileName, fdEntrySectionNumber, UE_SECTIONVIRTUALSIZE);
        fdEntrySectionOffset = (long)GetPE32Data(szFileName, fdEntrySectionNumber, UE_SECTIONVIRTUALOFFSET);
        if(!inFileStatus.FileIsDLL)
            fdProcessInfo = (LPPROCESS_INFORMATION)InitDebugEx(szFileName, NULL, NULL, (void*)cbEntry);
        else
        {
            MessageBoxA(shared, "DLL Files do not support nanomites...", "LOL", MB_ICONERROR);
            EnableWindow(GetDlgItem(shared, IDC_BTN_GO), 1);
            return 0;
        }
        if(fdProcessInfo)
        {
            DebugLoop();
            char str[100]="";
            sprintf(str, "%d", current_nanomite);
            SetDlgItemTextA(shared, IDC_STC_LOG, str);
            EnableWindow(GetDlgItem(shared, IDC_BTN_GO), 1);
            int fake=CleanupTable();
            sprintf(str, "%d real\n%d fake\n%d in total", NanoTable.total, fake, NanoTable.total+fake);
            MessageBoxA(shared, str, "Found", MB_ICONINFORMATION);
            SendMessageA(shared, WM_COMMAND, IDC_BTN_INIT, 1);
            return 0;
        }
        else
        {
            MessageBoxA(shared, "Something went wrong during initialization...", "Error!", MB_ICONERROR);
            EnableWindow(GetDlgItem(shared, IDC_BTN_GO), 1);
            return 0;
        }
    }
    else
    {
        MessageBoxA(shared, "This is not a valid PE file...", "Error!", MB_ICONERROR);
    }
    EnableWindow(GetDlgItem(shared, IDC_BTN_GO), 1);
    return 1;
}
